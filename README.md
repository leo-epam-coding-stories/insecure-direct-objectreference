# INSECURE DIRECT OBJECT REFERENCE
**To read**: Insecure Direct Object Reference (called IDOR from here) occurs when a application exposes a reference to an internal implementation object. 
Using this way, it reveals the real identifier and format/pattern used of the element in the storage backend side. 
The most common example of it (although is not limited to this one) is a record identifier in a storage system (database, filesystem and so on).

**Estimated reading time**: 12 Minutes

## Story Outline
The context of this story is to prudently carry the backend data, specifically with those with identifier information to the presentation tier such that there is no footprint left to the hackers to easily figure the pattern/format to access the data.


## Story Organization
**Story Branch**: master
> `git checkout master`

**Practical task tag for self-study**: task
> `git checkout task`

Tags: [OWASP|https://cheatsheetseries.owasp.org/cheatsheets/Insecure_Direct_Object_Reference_Prevention_Cheat_Sheet.html]