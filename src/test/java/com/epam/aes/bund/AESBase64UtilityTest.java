package com.epam.aes.bund;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class AESBase64UtilityTest {

	@Test
	void testEncryptNotNull() throws Exception {
		String customerValue = "CA-28478";
		String actual = AESBase64Utility.encrypt(customerValue.getBytes());
		assertNotNull(actual);
	}
	
	@Test
	@SuppressWarnings("deprecation")
	void testEncryptEqual() throws Exception {
		String customerValue = "CA-28478";
		String actual = AESBase64Utility.encrypt(customerValue.getBytes());
		String readableCustomerValue = AESBase64Utility.decrypt(actual);
		assertNotNull(actual,readableCustomerValue);
	}

}
