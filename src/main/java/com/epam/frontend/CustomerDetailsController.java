package com.epam.frontend;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.database.model.Customer;
import com.epam.database.service.CustomerService;

@RestController
public class CustomerDetailsController {
			
	private final CustomerService customerService;
	
	public CustomerDetailsController(CustomerService customerService) {
		this.customerService = customerService;
	}

	@RequestMapping(value = "/customers")
	public ResponseEntity<Object> getCustomers() {
		List<Customer> customerList = null;
		try {
			customerList = customerService.getAllCustomers();		
		}catch(Exception ex) {
			return new ResponseEntity<>( "INTERNAL_PROCESS_ERROR", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return new ResponseEntity<>(customerList, HttpStatus.OK);
	}
}
